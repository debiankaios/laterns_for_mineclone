local S = minetest.get_translator("mcl_latern")

minetest.register_node("mcl_latern:latern", { -- There are two version of screenshots, i like the which i have choose more
		tiles = {
				"latern_up.png", -- y+
				"latern_down.png", -- y-
				"latern.png", -- x+
				"latern.png", -- x-
				"latern.png", -- z+
				"latern.png", -- z-
		},
		groups = {pickaxey=3},
		inventory_image = "latern.png",
		light_source = 14,
		drop = "mcl_latern:latern",
		description = S("Latern"),
		drawtype = "nodebox",
		paramtype = "light",
		paramtype2 = "facedir",
    node_box = {
      type = "fixed",
	    fixed = {
				{-0.1875, -0.5000, -0.1875, 0.1875, -0.06250, 0.1875},
				{-0.1250, -0.06250, -0.1250, 0.1250, 0.06250, 0.1250},
				{-0.06250, 0.1250, -0.006250, 0.06250, 0.1875, 0.006250},
				{-0.06250, 0.06250, -0.006250, -0.03125, 0.1250, 0.006250},
				{0.03125, 0.06250, -0.006250, 0.06250, 0.1250, 0.006250},
	    }
    },
		stack_max = 64,
})



minetest.register_craft({
    type = "shaped",
    output = "mcl_latern:latern",
    recipe = {
        {"mcl_core:iron_nugget", "mcl_core:iron_nugget","mcl_core:iron_nugget"},
        {"mcl_core:iron_nugget", "mcl_torches:torch",  "mcl_core:iron_nugget"},
        {"mcl_core:iron_nugget", "mcl_core:iron_nugget",  "mcl_core:iron_nugget"}
    }
})
